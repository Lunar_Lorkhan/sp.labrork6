package com.labwork6.controller;

import com.labwork6.domain.CashCard;
import com.labwork6.domain.Employee;
import com.labwork6.domain.fictitiousDomains.ClientInfo;
import com.labwork6.repos.ClientRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/")
public class ClientController {
    private final ClientRepository repository;

    ClientController(ClientRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/clientCards/{clientNo}")
    List<CashCard> allCashCard(@PathVariable("clientNo") Long clientNo) {
        return repository.findCashCards(clientNo);
    }

    @GetMapping("/getClientInfo/{clientNo}")
    List<ClientInfo> getClientInfo(@PathVariable("clientNo") Long clientNo) {
        return repository.findClientInfoByClientNo(clientNo);
    }
}
