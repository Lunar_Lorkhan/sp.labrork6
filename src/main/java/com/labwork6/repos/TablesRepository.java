package com.labwork6.repos;

import com.labwork6.domain.Tables;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TablesRepository extends JpaRepository<Tables, String> {
}
