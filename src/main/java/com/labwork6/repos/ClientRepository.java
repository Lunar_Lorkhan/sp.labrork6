package com.labwork6.repos;

import com.labwork6.domain.CashCard;
import com.labwork6.domain.Client;
import com.labwork6.domain.fictitiousDomains.ClientInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

// To read about it more https://habr.com/ru/post/435114/
// and https://stackoverflow.com/questions/36328063/how-to-return-a-custom-object-from-a-spring-data-jpa-group-by-query/36329166#36329166
public interface ClientRepository extends JpaRepository<Client, Long> {
    @Query("SELECT card FROM CashCard card " +
            "JOIN CardClientTable cct ON card = cct.cardNumber " +
            "WHERE cct.clientNo.clientNo = :clientno")
    List<CashCard> findCashCards(@Param("clientno") Long clintno);

    @Query("SELECT new com.labwork6.domain.fictitiousDomains.ClientInfo(client.fName, client.lName, client.telNumber, cct.cardNumber.cardNumber, orders.orderNo) " +
            "FROM Client client " +
            "LEFT JOIN CardClientTable cct ON cct.clientNo = client " +
            "LEFT JOIN Orders orders ON orders.clientNo = client " +
            "WHERE client.clientNo = :clientNo")
    List<ClientInfo> findClientInfoByClientNo(@Param("clientNo") Long clientNo);
}
