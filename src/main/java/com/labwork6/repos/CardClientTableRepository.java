package com.labwork6.repos;

import com.labwork6.domain.CardClientTable;
import com.labwork6.domain.complexKey.PrimaryKeyForCardClientTable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CardClientTableRepository extends JpaRepository<CardClientTable, PrimaryKeyForCardClientTable> {
}
