package com.labwork6.repos;

import com.labwork6.domain.CashCard;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CashCardRepository extends JpaRepository<CashCard, String> {
}
