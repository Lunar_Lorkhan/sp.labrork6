package com.labwork6.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@ToString
@Table(name = "tables")
public class Tables {
    @Id
    @Column(name = "tableno")
    private String tableNo;

    @Column(name = "numofseats")
    private Integer numOfSeats;
}
