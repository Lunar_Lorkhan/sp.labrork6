package com.labwork6.domain.enums;

public enum CashCardPaymentSystem {
    visa,
    mastercard,
    mir
}
