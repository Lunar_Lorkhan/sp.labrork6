package com.labwork6.domain.enums;

public enum EmployeePosition {
    supervisor,
    waiter
}
