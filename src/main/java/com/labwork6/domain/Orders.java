package com.labwork6.domain;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@RequiredArgsConstructor
@Table(name = "orders")
public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "orderno")
    private Long orderNo;

    @NonNull
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "dishno")
    private Menu dishNo;

    @NonNull
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "clientno")
    private Client clientNo;

    @Column(name = "options")
    private String options;

    @CreationTimestamp
    @Column(name = "dateandtime")
    private ZonedDateTime dateAndTime;
}
