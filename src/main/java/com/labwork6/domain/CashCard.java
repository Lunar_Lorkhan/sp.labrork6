package com.labwork6.domain;

import com.labwork6.domain.enums.CashCardPaymentSystem;
import com.labwork6.domain.enums.CashCardType;
import lombok.*;

import javax.persistence.*;

@Entity
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "cashcard")
public class CashCard {
    @Id
    @Column(name = "cardnumber")
    private String cardNumber;

    @Column(name = "paymentsystem")
    @Enumerated(EnumType.STRING)
    private CashCardPaymentSystem paymentSystem;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private CashCardType type;
}
