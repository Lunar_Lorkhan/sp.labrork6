package com.labwork6.domain;

import com.labwork6.domain.enums.EmployeePosition;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "empno")
    private Long empNo;

    @Column(name = "fname")
    private String fName;

    @Column(name = "lname")
    private String lName;

    @Enumerated(EnumType.STRING)
    @Column(name = "position")
    private EmployeePosition position;

    @Column(name = "passser")
    private String passser;

    @Column(name = "passnum")
    private String passnum;
}
