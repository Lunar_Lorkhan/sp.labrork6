package com.labwork6.domain.fictitiousDomains;

import com.labwork6.domain.CashCard;
import com.labwork6.domain.Orders;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ClientInfo {
    private String fName;
    private String lName;
    private String telNumber;
    private String cashCard;
    private Long orders;
}
