package com.labwork6.domain;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "bill")
public class Bill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "billno")
    private Long billNo;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "clientno")
    private Client clientNo;

    @Column(name = "paymenttype")
    private String paymentType;

    @Column(name = "orderamount")
    private Integer orderAmount;

    @CreationTimestamp
    @Column(name = "dateandtime")
    private ZonedDateTime dateAndTime;
}
