package com.labwork6.domain.complexKey;

import com.labwork6.domain.CashCard;
import com.labwork6.domain.Client;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@EqualsAndHashCode
@ToString
public class PrimaryKeyForCardClientTable implements Serializable {
    static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private Client clientNo;

    @Getter
    @Setter
    private CashCard cardNumber;
}
