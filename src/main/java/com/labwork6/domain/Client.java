package com.labwork6.domain;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "client")
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "clientno")
    private Long clientNo;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "empno")
    private Employee empNo;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "tableno")
    private Tables tableNo;

    @Column(name = "telnumber")
    private String telNumber;

    @Column(name = "fname")
    private String fName;

    @Column(name = "lname")
    private String lName;
}
