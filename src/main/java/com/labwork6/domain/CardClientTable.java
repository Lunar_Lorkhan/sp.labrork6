package com.labwork6.domain;

import com.labwork6.domain.complexKey.PrimaryKeyForCardClientTable;
import lombok.*;

import javax.persistence.*;

// Для реализации составного первичного ключа необходимо создать
// специальный класс.
@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "cardclienttable")
@IdClass(PrimaryKeyForCardClientTable.class)
public class CardClientTable {
    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "clientno")
    private Client clientNo;

    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cardnumber")
    private CashCard cardNumber;
}
