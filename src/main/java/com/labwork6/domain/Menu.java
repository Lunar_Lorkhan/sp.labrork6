package com.labwork6.domain;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "menu")
public class Menu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dishno")
    private Long dishNo;

    @Column(name = "dishname")
    private String dishName;

    @Column(name = "cousinefrom")
    private String cousineFrom;

    @Column(name = "price")
    private Integer price;

    @Column(name = "dishtype")
    private String dishType;
}
